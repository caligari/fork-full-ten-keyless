/* Copyright 2022 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "neopixel_spi.hh"
#include "Arduino.h"

static uint8_t colors[9];
volatile uint8_t bit_counter;
volatile uint8_t color_counter;
volatile uint8_t *color_pointer;
volatile uint8_t color_value;
static uint8_t r, g, b;
static VAR_WAITER(waiterColor);
static VAR_WAITER(waiterInt);

ISR(SPI_STC_vect, ISR_NAKED) {
    asm volatile (
                    "push r18\n\t"
                    "in   r18, 0x3F\n\t"
                    "push r18\n\t"
                    "push r19\n\t"
                    "ldi  r19, 0x84\n\t"
                    "lds  r18, color_value\n\t"
                    "sbrc r18, 7\n\t"
                    "ori  r19, 0x40\n\t"
                    "sbrc r18, 6\n\t"
                    "ori  r19, 0x02\n\t"
                    "out  0x2E, r19\n\t" // 0x2E -> SPDR
                    "lds  r19, bit_counter\n\t"
                    "subi r19, 1\n\t"
                    "breq byte_sent\n\t"
                    "sts  bit_counter, r19\n\t"
                    "add  r18, r18\n\t"
                    "add  r18, r18\n\t"
                    "sts  color_value, r18\n\t" // color_value <<= 2
                    "pop  r19\n\t"
                    "pop  r18\n\t"
                    "out  0x3F, r18\n\t"
                    "pop  r18\n\t"
                    "reti \n\t"
    "byte_sent:      lds  r19, color_counter\n\t"
                    "subi r19, 1\n\t"
                    "breq data_sent\n\t"
                    "sts color_counter, r19\n\t"
                    "push r30\n\t"
                    "push r31\n\t"
                    "lds  r30, color_pointer\n\t"
                    "lds  r31, color_pointer+1\n\t"
                    "ldd  r19, Z+0\n\t"
                    "adiw r30, 1\n\t" // for some reason, ldd r19,Z+ doesn't compile as post-increment
                    "sts  color_value, r19\n\t"
                    "sts  color_pointer, r30\n\t"
                    "sts  color_pointer+1, r31\n\t"
                    "ldi  r19, 4\n\t"
                    "sts  bit_counter, r19\n\t"
                    "pop  r31\n\t"
                    "pop  r30\n\t"
                    "pop  r19\n\t"
                    "pop  r18\n\t"
                    "out  0x3F, r18\n\t"
                    "pop  r18\n\t"
                    "reti\n\t"
    "data_sent:      in   r18, 0x2C\n\t"
                    "andi r18, 0x7F\n\t"
                    "out  0x2C, r18\n\t" // SPCR &= ~(1<<SPIE)
                    "push r0\n\t"
                    "push r1\n\t"
                    "eor  r1, r1\n\t"
                    "push r20\n\t"
                    "push r21\n\t"
                    "push r22\n\t"
                    "push r23\n\t"
                    "push r24\n\t"
                    "push r25\n\t"
                    "push r26\n\t"
                    "push r27\n\t"
                    "push r28\n\t"
                    "push r29\n\t"
                    "push r30\n\t"
                    "push r31\n\t");

    VarWaiterNotifyISR(&waiterInt);

    asm volatile(   "pop  r31\n\t"
                    "pop  r30\n\t"
                    "pop  r29\n\t"
                    "pop  r28\n\t"
                    "pop  r27\n\t"
                    "pop  r26\n\t"
                    "pop  r25\n\t"
                    "pop  r24\n\t"
                    "pop  r23\n\t"
                    "pop  r22\n\t"
                    "pop  r21\n\t"
                    "pop  r20\n\t"
                    "pop  r1\n\t"
                    "pop  r0\n\t"
                    "pop  r19\n\t"
                    "pop  r18\n\t"
                    "out  0x3F, r18\n\t"
                    "pop  r18\n\t"
                    "reti \n\t");
}

static ASYNC_FUNC(neopixelLoop) {

    BEGIN_ASYNC();
    SPDR = 0; // set the SPI line to 0
    DELAY(10); // force a reset
    while(true) {
        WAITFOR(&waiterColor);
        colors[0] = colors[3] = colors[6] = g;
        colors[1] = colors[4] = colors[7] = r;
        colors[2] = colors[5] = colors[8] = b;
        bit_counter = 4;
        color_counter = 9;
        color_pointer = colors+1;
        color_value = colors[0];
        SPSR = 0;
        SPCR |= (1<<SPIE);
        WAITFOR(&waiterInt);
        DELAY(10);
    }
    END_ASYNC();
}

void SK6812NeoPixelInit() {
    r = g = b = 0;
    /* Set PB2/MOSI and PB0/SS as output */
    DDRB |= (1<<PB2)|(1<<PB0);
    PORTB &= ~(1<<PB2);
    /* Enable SPI, Master, set clock rate fck/4, MODE1 */
    SPCR = (1<<SPE)|(1<<MSTR)|(1<<CPHA);
    SPSR = 0;
    // launch the LEDs thread
    neopixelLoop(NULL);
}

void SK6812NeoPixelSetColor(uint8_t ir, uint8_t ig, uint8_t ib) {
    r = ir;
    g = ig;
    b = ib;
    VarWaiterNotify(&waiterColor);
}
