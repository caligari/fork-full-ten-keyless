/* Copyright 2023 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "macros.hh"
#include <Arduino.h>

// list of macros
MACRO macro_list[] = {
    {KEY_UP, "\x80"}, // \x80 -> volume up
    {KEY_RIGHT, "\x80"},
    {KEY_DOWN, "\x81"}, // \x81 -> volume down
    {KEY_LEFT, "\x81"},
    {KEY_DELETE, "\x82"}, // \x82 -> volume mute/unmute
    {0xFFFF, NULL}
};

struct _key_entry {
    uint8_t character;
    uint8_t keystrokes[2];
};

typedef struct _key_entry KEY_ENTRY;

uint8_t *macro_buffer = NULL;

// spanish layout
static const KEY_ENTRY key_layout[] = {
    // number < 0x80: key index (check common.cpp for it in kbdScancodes)
    // 0xDA = modifier AltGr (AltGr index + 0x80)
    // 0xC0 = modifier Left Shift (Left Shift index + 0x80)
    {'\\', {0xDA, 0x10}},
    {'1', {0x11}}, {'!', {0xC0, 0x11}}, {'|', {0xDA, 0x11}},
    {'2', {0x12}}, {'"', {0xC0, 0x12}}, {'@', {0xDA, 0x12}},
    {'3', {0x13}}, {'#', {0xDA, 0x13}},
    {'4', {0x14}}, {'$', {0xC0, 0x14}}, {'~', {0xDA, 0x14}},
    {'5', {0x15}}, {'%', {0xC0, 0x15}},
    {'6', {0x16}}, {'&', {0xC0, 0x16}},
    {'7', {0x17}}, {'/', {0xC0, 0x17}},
    {'8', {0x18}}, {'(', {0xC0, 0x18}},
    {'9', {0x19}}, {')', {0xC0, 0x19}},
    {'0', {0x1A}}, {'=', {0xC0, 0x1A}},
    {'\'', {0x1B}}, {'?', {0xC0, 0x1B}},

    {'q', {0x21}},
    {'w', {0x22}},
    {'e', {0x23}},
    {'r', {0x24}},
    {'t', {0x25}},
    {'y', {0x26}},
    {'u', {0x27}},
    {'i', {0x28}},
    {'o', {0x29}},
    {'p', {0x2A}},
    {'`', {0x2B}}, {'^', {0xC0, 0x2B}}, {'[', {0xDA, 0x2B}},
    {'+', {0x2C}}, {'*', {0xC0, 0x2C}}, {']', {0xDA, 0x2C}},
    {'\n', {0x2D}},

    {'a', {0x31}},
    {'s', {0x32}},
    {'d', {0x33}},
    {'f', {0x34}},
    {'g', {0x35}},
    {'h', {0x36}},
    {'j', {0x37}},
    {'k', {0x38}},
    {'l', {0x39}},
    {'{', {0xDA, 0x3B}},
    {'}', {0xDA, 0x3C}},

    {'<', {0x41}}, {'>', {0xCA, 0x41}},
    {'z', {0x42}},
    {'x', {0x43}},
    {'c', {0x44}},
    {'v', {0x45}},
    {'b', {0x46}},
    {'n', {0x47}},
    {'m', {0x48}},
    {',', {0x49}}, {';', {0xC0, 0x49}},
    {'.', {0x4A}}, {':', {0xC0, 0x4A}},
    {'-', {0x4B}}, {'_', {0xC0, 0x4B}},
    {' ', {0x56}},
    {0, {0}},
};

ASYNC_FUNC(send_macro_buffer) {

    static uint8_t modifier;
    static uint8_t keystroke;
    static uint8_t character;
    static const KEY_ENTRY *key_entry;

    BEGIN_ASYNC();
    for (;*macro_buffer; macro_buffer++) {
        modifier = 0xFF;
        character = *macro_buffer;
        // Can't use SWITCH inside an asynchronous function, unless there are no asynchronous calls inside.
        // But in this case we have DELAY.
        if (character == 0x80) { // volume up
            Keyboard.press(KEY_MEDIA_VOLUME_INC);
            DELAY(MACROS_KEYPRESS_DELAY);
            Keyboard.release(KEY_MEDIA_VOLUME_INC);
            DELAY(MACROS_KEYPRESS_DELAY);
            continue;
        }
        if (character == 0x81) { // volume down
            Keyboard.press(KEY_MEDIA_VOLUME_DEC);
            DELAY(MACROS_KEYPRESS_DELAY);
            Keyboard.release(KEY_MEDIA_VOLUME_DEC);
            DELAY(MACROS_KEYPRESS_DELAY);
            continue;
        }
        if (character == 0x82) { // volume mute/unmute
            Keyboard.press(KEY_MEDIA_MUTE);
            DELAY(MACROS_KEYPRESS_DELAY);
            Keyboard.release(KEY_MEDIA_MUTE);
            DELAY(MACROS_KEYPRESS_DELAY);
            continue;
        }
        if ((character >= 'A') && (character <= 'Z')) {
            character |= 0x20;
            modifier = 0x40;
        }
        keystroke = 0xFF;
        for (key_entry = key_layout; key_entry->character; key_entry++) {
            if (key_entry->character == character) {
                if (key_entry->keystrokes[0] >= 0x80) { // has a modifier key
                    modifier = key_entry->keystrokes[0] & 0x7F;
                    keystroke = key_entry->keystrokes[1];
                } else { // don't touch the modifier because it could have been set by an uppercase letter
                    keystroke = key_entry->keystrokes[0];
                }
                break;
            }
        }
        if (keystroke == 0xFF) { // not found
            continue;
        }
        if (modifier != 0xFF) {
            Keyboard.press(kbdScancodes[modifier]);
            DELAY(MACROS_KEYPRESS_MODIFIER_DELAY);
        }
        Keyboard.press(kbdScancodes[keystroke]);
        DELAY(MACROS_KEYPRESS_DELAY);
        Keyboard.release(kbdScancodes[keystroke]);
        DELAY(MACROS_KEYPRESS_DELAY);
        if (modifier != 0xFF) {
            Keyboard.release(kbdScancodes[modifier]);
            DELAY(MACROS_KEYPRESS_MODIFIER_DELAY);
        }
    }
    macro_buffer = NULL;
    END_ASYNC();
}
