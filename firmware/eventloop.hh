/* Copyright 2022 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _EVENTLOOP_H_
#define _EVENTLOOP_H_

#include "common.hh"

#define ASMTAB "\n\t"

struct _event_st {
    void (*callback)(_event_st *);
    struct _event_st *next;
    struct _event_st *data;
    void *line;
    uint64_t delay;
};

typedef struct _event_st Event;
typedef void (*CallBack)(Event *);

typedef struct {
    Event *head;
    Event *tail;
} EventQueue;

#define EVENT_QUEUE(NAME) EventQueue NAME = {NULL, NULL};

typedef struct {
    Event *waiter;
    bool changed;
} VarWaiter;

#define VAR_WAITER(NAME) VarWaiter NAME = {NULL, NULL};

void QueueInsertEvent(EventQueue *, Event *);
void QueueInsertEventOrdered(EventQueue *queue, Event *event);
Event *QueueRemoveEvent(EventQueue *);
// it's simpler to just do it this way instead of creating a function
#define QueuePeekEvent(EVENT_QUEUE) ((EVENT_QUEUE)->head)

void EventLoopInit(void);
void EventLoopRun(void);
void EventLoopAddDelay(Event *event, int timeout);
void EventLoopInsertEventISR(Event *event);
void EventLoopInsertEvent(Event *event);

void VarWaiterWait(VarWaiter *waiter, Event *event);
void VarWaiterNotifyISR(VarWaiter *waiter);
void VarWaiterNotify(VarWaiter *waiter);

// coroutines macros

#define CONCAT_IMPL(a, b) a##b
#define CONCAT(a, b) CONCAT_IMPL(a, b)

#define ASYNC_FUNC(FUNC_NAME) void FUNC_NAME(Event *event) {\
    static Event _innerEvent;\
    static EVENT_QUEUE(_innerEventQueue);\
    static bool _working = false;\
    _innerEvent.callback = FUNC_NAME;

#define BEGIN_ASYNC() if (_working && (event != &_innerEvent)) {\
            QueueInsertEvent(&_innerEventQueue, event);\
            return;\
        }\
        do {\
            _working = true;\
            if (event == &_innerEvent) {\
                event = event->data;\
            } else {\
                _innerEvent.line = 0;\
                _innerEvent.delay = 0;\
                _innerEvent.data = event;\
            }\
            if (_innerEvent.line) { goto *_innerEvent.line; }

#define END_ASYNC() EventLoopInsertEvent(event);\
            _working = false;\
            event = QueueRemoveEvent(&_innerEventQueue);\
        } while(event);\
        return;\
    }

#define ASYNC_FUNC_DECL(FUNC_NAME) void FUNC_NAME(Event *)

#define DELAY(TIMEOUT) _innerEvent.line=&&CONCAT(line,__LINE__);EventLoopAddDelay(&_innerEvent, TIMEOUT); return; CONCAT(line,__LINE__):;
#define PERIODIC_DELAY(TIMEOUT) _innerEvent.line=&&CONCAT(line,__LINE__);EventLoopAddDelay(&_innerEvent, TIMEOUT); return; CONCAT(line,__LINE__):;
#define YIELD(FUNCTION) _innerEvent.line=&&CONCAT(line,__LINE__);FUNCTION(&_innerEvent); return; CONCAT(line,__LINE__):;
#define WAITFOR(AWAITER) _innerEvent.line=&&CONCAT(line,__LINE__);VarWaiterWait(AWAITER, &_innerEvent); return; CONCAT(line,__LINE__):;
#define ASYNC_RETURN() if (event!=&_innerEvent) EventLoopInsertEvent(event); _working = false; event = QueueRemoveEvent(&_innerEventQueue); continue;

#endif // _EVENTLOOP_H_
